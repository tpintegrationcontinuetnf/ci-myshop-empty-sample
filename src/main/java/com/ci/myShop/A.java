package com.ci.myShop;

public class A {
	private String name;
	private String login;
	private int age;
	
	
	public A (String name, String login, int age) {
		
		this.name = name;
		this.login = login;
		this.age = age;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLoginString() {
		return login;
	}


	public void setLoginString(String login) {
		this.login = login;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}
	
}
