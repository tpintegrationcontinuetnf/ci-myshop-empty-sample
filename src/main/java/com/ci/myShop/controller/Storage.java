package com.ci.myShop.controller;


import java.util.HashMap;
import java.util.Map;

import com.ci.myShop.model.Item;

public class Storage {

	
	private Map<String, Item> itemMap;
	
	public Storage() {
		
		itemMap = new HashMap<String,Item>();
	}
	
	
	public void addItem(Item item) {
	itemMap.put(item.getName(), item);
	}
	
	public Item getItem(String name) {
		return itemMap.get(name);
			}


	public Map<String, Item> getItemMap() {
		return itemMap;
	}

	public void setItemMap(Map<String, Item> itemMap) {
		this.itemMap = itemMap;
	}	
	

}


