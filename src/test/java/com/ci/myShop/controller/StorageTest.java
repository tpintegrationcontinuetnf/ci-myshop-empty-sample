package com.ci.myShop.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ci.myShop.model.Item;

public class StorageTest {

	@Test
	public void addItem() {
		Storage storage=new Storage();
		Item it1=new Item("test", (int) 100f, 5, 100);
		storage.addItem(it1);
		assertEquals(1,storage.getItemMap().values().size());
	}
	
	@Test
	public void getItem() {
		Storage storage =new Storage();
		Item it1=new Item("test", (int) 100f, 5, 100);
		storage.addItem(it1);
		Item extractedItem=storage.getItem("test");
		assertEquals(1,storage.getItemMap().values().size());
	}
	@Test
	public void getNbrElt() {
		Item item = new Item("test", (int) 100f, 5, 100);
		item.setNbrElt(2);
		assertEquals(2,item.getNbrElt());
		
				
	}
		
	
	
}
