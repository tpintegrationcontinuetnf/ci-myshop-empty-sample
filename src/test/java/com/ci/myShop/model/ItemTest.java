package com.ci.myShop.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class ItemTest {

	@Test
	public void createEmptyItem() {
		Item item=new Item();
		assertNotEquals(null,item);
		
	}
	
	@Test
	
	public void updateParameters() {
		Item item = new Item("name", 100, 5, 10);
		assertEquals("name", item.getName());
	}
	
	@Test
	public void setPrice() {
		Item item = new Item("name", 100, 5, 10);
		assertEquals(5f,item.getPrice(),0.00001);
		item.setPrice(10);
		assertEquals(10f, item.getPrice(),0.00001);
	}
}

